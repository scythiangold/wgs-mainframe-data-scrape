Attribute VB_Name = "Module1"
'   Declare variables
    Option Explicit
    Const WaitValue = 50
    Private Const WM_GETTEXT As Integer = &HD
    Private Const WM_GETTEXTLENGTH = &HE
    Private Const WM_SETTEXT As Long = &HC
    Private Const WM_KEYDOWN As Long = &H100
    Private Const WM_KEYUP As Long = &H101
    Private Const WM_CLOSE As Long = &H10
    Private Const HWND_BROADCAST As Long = &HFFFF
    Private Const PROCESS_QUERY_INFORMATION = &H400
    Private Const PROCESS_TERMINATE = 1
    Private Sessions As Object
    Private System As Object
    Private Session As Object
    Private OldSystemTimeout&
    Private pid As Long
    Private hWnd As Long
    Private CursorText As String
    Private LLFound As String
    Private ClaimDate As Long
    Private EffectDate As Long
    Private EndDate As Long
    Private ClaimStatus As String
    Private InsuranceCode As String
    Private InsurerName As String
    Private COBType As String
    Private ClaimNumber As String
    Private MenuOption As String
    Private CarrierName As String
    Private AllowedAmount As String
    Private AllowedAmountNum As Single
    Private RawString As String
'   Declare Sub WaitHost()
'   Declare Sub LLScreen()
'   Declare Sub CBScreen()
'   Private Declare Function ConvertString$(RawString)
'   Private Declare Function ConvertDate(RawString)
    Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    Private Declare Function GetDlgItem Lib "user32.dll" (ByVal hDlg As Long, ByVal nIDDlgItem As Long) As Long
    Private Declare Function FindWindowEx Lib "user32.dll" Alias "FindWindowExA" (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpsz1 As String, ByVal lpsz2 As String) As Long
    Private Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
'   Private Declare Function SendNotifyMessage Lib "user32.dll" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
'   Private Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long
    Private Declare Function PostThreadMessage Lib "user32.dll" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long
    Private Declare Function OpenProcess Lib "kernel32.dll" (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
    Private Declare Function TerminateProcess Lib "kernel32.dll" (ByVal hProcess As Long, ByVal uExitCode As Long) As Long
    Private Declare Function CloseHandle Lib "kernel32.dll" (ByVal hObject As Long) As Long
'   Private Declare Function ApplicationActive Lib "kernel32.dll" (strApp As String) As Boolean
    Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

Sub Main()

'   Set variables
    Dim x As Integer
    Dim y As Integer
    Dim Row As Integer
    Dim Col As Integer
    Dim rVal As Long
    Dim Counter As Long
    Dim SourcePathName As String
    Dim DestPathName As String
    Dim StartLine As Long
    Dim Overwrite As Integer
    Dim PolicyNumber As String
    Dim CBFound As String
    Dim DateFlag As Integer
    Dim StringLength As Integer
    
    Application.EnableCancelKey = xlInterrupt
    
    pid = 177472
    hWnd = 170664
    
'   Create object
    Set System = CreateObject("EXTRA.System")
    Set Sessions = System.Sessions
    Set Session = System.ActiveSession
    
'   Handle error if object cannot be created
    If Session Is Nothing Then
    
        MsgBox ("Could not create the Session object.  Stopping macro playback.")
        Exit Sub
        
    End If

'   Set variable
    Counter = Application.ThisWorkbook.Worksheets("Sheet1").Cells(Rows.Count, 3).End(xlUp).Row + 1
    MsgBox (Counter)
    
'   Loop until end of source data file is reached
    Do Until Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 1).Value = ""
    
'       Set variables
        LLFound = "False"
        CBFound = "False"
        ClaimStatus = ""
        CarrierName = ""
        AllowedAmount = ""
    
'       Retreive policy number and claim number from source
        PolicyNumber = Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 1).Value
        ClaimNumber = Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 2).Value
        
'       Remove trailing spaces from policy number and claim number
        PolicyNumber = RTrim(PolicyNumber)
        ClaimNumber = RTrim(ClaimNumber)
        
'       Send policy number to session
        Session.Screen.PutString PolicyNumber, 10, 35
        Call WaitHost
        
'       Navigate session
        Session.Screen.SendKeys ("<Enter>")
        Call WaitHost
        
'       Retrieve page indentifier from session
        CursorText = Session.Screen.Getstring(1, 3, 8)
        
'       Navigate session
        If CursorText = "GNCDCNDI" Then
        
            ClaimStatus = "DUPLICATE DCN FOUND"
            Session.Screen.SendKeys ("<S>")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost
            
        End If
        
'       Retrieve page identifier from session
        CursorText = Session.Screen.Getstring(1, 3, 8)
        
'       Navigate session
        If CursorText = "GNCACLM " Then
        
            Session.Screen.SendKeys ("<Tab>")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost
            
        End If
        
'       Retrieve page identifier from session
        CursorText = Session.Screen.Getstring(1, 3, 8)
        
'       Navigate session
        If CursorText = "GNCLMBRE" Then
            
            Session.Screen.SendKeys ("0")
            Call WaitHost
            
            Session.Screen.SendKeys ("1")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost
            
        End If
        
'       Retrieve page identifier from session
        CursorText = Session.Screen.Getstring(1, 3, 8)
        
'       Navigate session
        If CursorText = "GNCACLM " Then
        
            Session.Screen.SendKeys ("<Tab>")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost

        End If

'       Navigate session
        Session.Screen.SendKeys ("<Pf11>")
        Call WaitHost
        
'       Set variables
        y = 7
        DateFlag = 0
        
'       Navigate session
        If Val(ClaimNumber) > 6 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
'       Navigate Session
        If Val(ClaimNumber) > 12 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
'       Navigate session
        If Val(ClaimNumber) > 18 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
'       Navigate session
        If CursorText = "WGMMENU " Then
        
            Session.Screen.SendKeys ("<Tab>")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost

        End If

'           Loop until date of claim number found
            Do
                
'               Retrieve heading text
                CursorText = Session.Screen.Getstring(y, 5, 4)
                
'               Confirm correct heading text
                If CursorText = "DATE" Then
        
'                   Loop until
                    Do
                    
'                       Retrieve index of claim date claim number
                        y = y + 1
                        CursorText = Session.Screen.Getstring(y, 2, 2)
        
'                       Compare index of claim number to claim date claim number
                        If Val(CursorText) = Val(ClaimNumber) Then

'                           Compare claim number to claim date claim number retreive claim date and set found flag to true
                            CursorText = Session.Screen.Getstring(y, 5, 6)
                            ClaimDate = ConvertDate(CursorText)
                            DateFlag = 1
                            Exit Do
                         
'                       Exit loop if claim number claim date not found
                        ElseIf CursorText = "__" Or CursorText = "LN" Then
            
                            DateFlag = 1
                            Exit Do
        
                        End If
                
                    Loop
                
                End If
                
'           Navigate session
            If CursorText = "GNCACLM " Then
        
                Session.Screen.SendKeys ("<A>")
                Call WaitHost
                
                Session.Screen.SendKeys ("<A>")
                Call WaitHost
            
                Session.Screen.SendKeys ("<Enter>")
                Call WaitHost

            End If
                
'               Increment y-axis coordinate and exit loop if found flag equals true
                y = y + 1
                If DateFlag = 1 Or y = 16 Then Exit Do
        
            Loop
        
'       Navigate to Limited Liability page using hidden shortcut
        Session.Screen.PutString "26", 24, 78
        Call WaitHost
        
        Session.Screen.MoveTo 24, 78
        Call WaitHost
        
        Session.Screen.SendKeys ("<Enter>")
        Call WaitHost
        
        If ClaimStatus <> "DUPLICATE DCN FOUND" Then Call LLScreen
        
'       Set cursor axis coordinates
        y = 21
        x = 4
        
'       Move cursor to coordinate position and retrieve page indentifier
        Session.Screen.MoveTo y, x
        Call WaitHost
        
        CursorText = Session.Screen.Getstring(y, x, 2)
        Call WaitHost
        
'       Navigate session
        If CursorText = "__" Or CursorText = "_ " Or CursorText = " _" Or CursorText = "  " Or CursorText = " P" Then y = y + 1
        
        CursorText = Session.Screen.Getstring(y, x, 2)
        
        If CursorText = "P " Then y = 18
        
'       Loop until
        Do

'           Exit loop if claim status is duplicate or no fault
            If ClaimStatus = "DUP IN SERVICE" Or ClaimStatus = "NO FAULT" Or ClaimStatus = "DUPLICATE DCN FOUND" Then Exit Do
        
'           Move cursor to coordinate position and retrieve COB type
            Session.Screen.MoveTo y, x
            CursorText = Session.Screen.Getstring(y, x, 2)
            
'           Set found flag to true, navigate to page and call subroutine if COB page found
            If CursorText = "CB" Then
            
                Let CBFound = "True"
                Session.Screen.SendKeys ("<Enter>")
                Call WaitHost
                Call CBScreen
                
                If LLFound = "True" And CBFound = "True" Then ClaimStatus = "LL AND CB FOUND"
                If LLFound = "False" And CBFound = "True" Then ClaimStatus = "LL NOT FOUND"
                Exit Do
            
'           Exit loop if claim status equals Medicare
            ElseIf CursorText = "MC" Then
                
                Let ClaimStatus = "MEDICARE"
                Exit Do
            
'           Set CB found flag to false and exit loop if COB page not found
            ElseIf CursorText = "  " Then
                
                If LLFound = "True" And CBFound = "False" Then ClaimStatus = "CB NOT FOUND"
                If LLFound = "False" And CBFound = "False" Then ClaimStatus = "LL AND CB NOT FOUND"
                Exit Do
                
            Else
                
'               Increment x-coordinate
                x = x + 3
            
            End If
            
        Loop
        
'       Navigate session
        Session.Screen.SendKeys ("<Pf3>")
        Call WaitHost
        
'       Navigate session
        Session.Screen.SendKeys ("<Pf3>")
        Call WaitHost
        
'       Retrieve page identifier from session
        CursorText = Session.Screen.Getstring(1, 3, 8)
        
'       Navigate session
        If CursorText = "GNCACLM " Then
        
'           Session.Screen.SendKeys ("<Pf3>")
'           Call WaitHost

            Session.Screen.SendKeys ("<Tab>")
            Call WaitHost
            
            Session.Screen.SendKeys ("<Enter>")
            Call WaitHost

        End If
        
'       Write data retrieved from session to destination
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 3).Value = ClaimStatus
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 4).Value = InsuranceCode
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 5).Value = InsurerName
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 6).Value = COBType
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 7).Value = CarrierName
        Application.ThisWorkbook.Worksheets("Sheet1").Cells(Counter, 8).Value = AllowedAmount
        
        If Counter Mod 100 = 0 Then
        
            Application.ThisWorkbook.Worksheets("Sheet1").Cells(1, 9).Value = Time
            Application.ThisWorkbook.Save
            
'           If ApplicationActive("Idle timer expired") = True Then SendKeys ("<Enter>")

'           AppActivate ("Idle timer expired", 50)
'           SendKeys ("<Enter>")
            
        End If
        
'       Increment variable
        Counter = Counter + 1

    Loop

'   Close session
    Session.Connected = False
    System.TimeoutValue = OldSystemTimeout
    
'   Clear object
    Set Sessions = Nothing
    Set System = Nothing
    Set Session = Nothing
    
'   Handler:

'   Write error code to destination data file
    Application.ThisWorkbook.Worksheets("Sheet1").Cells(1, 9).Value = Time
    Application.ThisWorkbook.Save
'   MsgBox ("Could not locate the source filename.  Stopping macro playback.")
    MsgBox ("Error")
    Reset
    
End Sub

Public Sub WaitHost()
    
'   Wait for constant time value before stepping to next instruction after sending tab or return keystroke
'   Application.Wait (Now() + TimeValue("00:00:01"))
'   Sleep 250
    Session.Screen.WaitHostQuiet (WaitValue)
    
End Sub

Public Sub LLScreen()

'   Declare variables
    Dim x As Integer
    Dim y As Integer
    Dim NewScreen As Integer
    Dim Counter As Integer
    
'   Set variables
    y = 8
    EffectDate = 0
    EndDate = 0
    InsuranceCode = ""
    InsurerName = ""
    COBType = " "
    NewScreen = -1

'   Check for available records and set flag to true if records found
    CursorText = Session.Screen.Getstring(23, 2, 19)
    
    If CursorText <> "SELECTION NOT FOUND" Then
    
        LLFound = "True"
        
        Do
        
            If y = 23 Then
        
                y = 8
                NewScreen = NewScreen + 1
                        
            End If
            
            y = y + 1
            
                For Counter = 0 To NewScreen
            
                    Session.Screen.SendKeys ("<Pf8>")
                    Call WaitHost
                
                    Let CursorText = Session.Screen.Getstring(23, 2, 9)
                
                    If CursorText = "LAST PAGE" Then Exit Do
                
                Next

                CursorText = Session.Screen.Getstring(y, 5, 3)
            
            If CursorText = "   " Then
                
                Exit Do

            ElseIf CursorText = "COB" Then
            
                    CursorText = Session.Screen.Getstring(y, 13, 8)
                
                    EffectDate = ConvertDate(CursorText)
                
                    CursorText = Session.Screen.Getstring(y, 22, 8)
                
                If CursorText = "        " Or CursorText = "99999999" Then CursorText = Date$
                
                Let EndDate = ConvertDate(CursorText)
                
                If EffectDate <> EndDate And ClaimDate >= EffectDate And ClaimDate <= EndDate Then
        
                    CursorText = Session.Screen.Getstring(y, 2, 2)
                    MenuOption = CursorText

                    Session.Screen.PutString MenuOption, 6, 17
                    Session.Screen.SendKeys ("<Enter>")
                    Call WaitHost
                
                    Let CursorText = Session.Screen.Getstring(9, 10, 10)
                    If CursorText <> "          " Then InsuranceCode = ConvertString(CursorText)
        
                    CursorText = Session.Screen.Getstring(9, 31, 40)
                    If CursorText <> "                                        " Then InsurerName = ConvertString(CursorText)

                    CursorText = Session.Screen.Getstring(8, 31, 1)
                    COBType = CursorText
                    
                    If COBType = "D" Then
                    
                        ClaimStatus = "DUP IN SERVICE"
                    
                    ElseIf COBType = "F" Then
                    
                        ClaimStatus = "NO FAULT"
                        
                    End If
                    
'                   Navigate session
                    Session.Screen.SendKeys ("<Pf3>")
                    Call WaitHost
                        
                    Exit Do
                
                End If

            End If
            
        Loop
        
    End If
    
'   Navigate session
    Session.Screen.SendKeys ("<Pf3>")
'   Call WaitHost
        
End Sub

Public Sub CBScreen()

'   Declare variables
    Dim ALFound As Integer
    Dim x As Integer
    Dim y As Integer
    
    If CarrierName = "" Then
    
        CursorText = Session.Screen.Getstring(6, 19, 25)
        
        If CursorText <> "_________________________" Or CursorText <> "                         " Then
        
            CarrierName = ConvertString(CursorText)
            
        Else
        
            CarrierName = ""
            
        End If
        
    End If
        
    CursorText = Session.Screen.Getstring(23, 28, 10)
        
    If CursorText = "PF09=DTLLN" Then
            
        Session.Screen.SendKeys ("<Pf9>")
        Call WaitHost
        
'       Set y-coordinate
        y = 8
                
        If Val(ClaimNumber) > 4 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
            
        If Val(ClaimNumber) > 8 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
        If Val(ClaimNumber) > 12 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
        If Val(ClaimNumber) > 16 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
        If Val(ClaimNumber) > 20 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
        If Val(ClaimNumber) > 24 Then
            
            Session.Screen.SendKeys ("<Pf8>")
            Call WaitHost
                
        End If
        
'       Set variable
        ALFound = 0
        
        Do
        
            CursorText = Session.Screen.Getstring(y, 3, 2)
        
            If Val(ClaimNumber) = Val(CursorText) Then
            
                CursorText = Session.Screen.Getstring(y, 27, 11)

                If CursorText = "___________" Or CursorText = "           " Then
            
                    AllowedAmount = ""
            
                Else
        
                    AllowedAmount = ConvertString(CursorText)
                    AllowedAmountNum = Val(AllowedAmount)
                    AllowedAmountNum = AllowedAmountNum * 0.01
                    AllowedAmount = LTrim(Str(AllowedAmountNum))
            
                End If
                
                ALFound = 1
                
'               Navigate session
                Session.Screen.SendKeys ("<Pf3>")
                Call WaitHost
                
                Exit Do
                
            ElseIf CursorText = "  " Then
            
                CursorText = Session.Screen.Getstring(y, 27, 11)
            
                If CursorText = "           " Then
                         
                    CursorText = Session.Screen.Getstring(24, 4, 43)
                    
                    If CursorText = "THIS CLAIM HAS NO DETAIL LINE LEVEL AMOUNTS" Then
                    
'                       Navigate session
                        Session.Screen.SendKeys ("<Pf3>")
'                       Call WaitHost
                        
                        CursorText = Session.Screen.Getstring(10, 19, 11)
                        
                        AllowedAmountNum = Val(CursorText)
                        AllowedAmountNum = AllowedAmountNum * 0.01
                        AllowedAmount = LTrim(Str(AllowedAmountNum))
                        
                    End If
                                
                Else
                
                    AllowedAmount = ""
                    
                    Session.Screen.SendKeys ("<Pf3>")
                    Call WaitHost
                            
                End If
                
                Exit Do
                
            Else
            
'               Increment y-coordinate
                y = y + 3
                
            End If
            
        Loop Until CursorText = "  " Or ALFound = 1
        
    End If
    
'   Navigate session
    Session.Screen.SendKeys ("<Pf3>")
    Call WaitHost
    
End Sub

Public Sub CloseDialog()
    
'       Application.ThisWorkbook.Worksheets("Sheet1").Cells(1, 10).Value = pid
        
'       KillProcess pid

'       SendNotifyMessage hWnd, WM_KEYDOWN, vbKeyTab, &H1C0001
'       SendNotifyMessage hWnd, WM_KEYUP, vbKeyTab, &HC11C0001
'       PostThreadMessage hWnd, WM_KEYDOWN, vbKeyTab, &HC11C0001
'       PostThreadMessage hWnd, WM_KEYUP, vbKeyTab, &HC11C0001
'       Application.Wait (Now() + TimeValue("00:00:01"))

'       SendNotifyMessage hWnd, WM_KEYDOWN, vbKeyTab, &H1C0001
'       SendNotifyMessage hWnd, WM_KEYUP, vbKeyTab, &HC11C0001
'       PostThreadMessage hWnd, WM_KEYDOWN, vbKeyReturn, &HC11C0001
'       PostThreadMessage hWnd, WM_KEYUP, vbKeyReturn, &HC11C0001
'       Application.Wait (Now() + TimeValue("00:00:01"))
        
End Sub

Function ConvertString$(RawString)

'   Declare variables
    Dim StringLength As Integer
    Dim NewString As String
    Dim Character(40) As String
    Dim Counter As Integer

'   Set variables
    StringLength = Len(RawString)
    NewString = ""
    
'   Loop until length of input string is exceeded and write deconstructed string to character array
    For Counter = 1 To StringLength
    
        Character(Counter - 1) = Mid(RawString, Counter, 1)
        
    Next

'   Loop until length of character array is exceeded and replace underscore with spaces
    For Counter = 0 To (StringLength - 1)
        
        If Character(Counter) = "_" Then Character(Counter) = " "
        
    Next
    
'   Loop until length of character array is excceded and reconstruct string
    For Counter = 0 To (StringLength - 1)
    
        NewString = NewString + Character(Counter)
        
    Next
    
'   Remove trailing spaces from string
    NewString = Trim(NewString)
    
'   Output reformatted string
    ConvertString = NewString
    
End Function

Function ConvertDate(RawString)

'   Declare variables
    Dim StringLength As Integer
    Dim Century As String
    Dim NewString As String
    Dim DateNumber As Long
    
'   Set variables
    StringLength = Len(RawString)
    Let Century = "20"
    
'   Parse date variable by day, month and year
    If StringLength = 8 Then
    
        Century = Mid(RawString, 5, 2)
        
    ElseIf StringLength = 10 Then
    
        Century = Mid(RawString, 7, 2)
        
    End If
    
'   Roll back century to avoid exceeding buffer size
'   Let Century = Trim(Str(Val(Century) - 1))
    
'   Determine format of date and strip out invalid characters
    If StringLength = 10 Then
    
        NewString = Left(RawString, 2) + "/" + Mid(RawString, 4, 2) + "/" + Century + Right(RawString, 2)

    Else
    
        NewString = Left(RawString, 2) + "/" + Mid(RawString, 3, 2) + "/" + Century + Right(RawString, 2)
        
    End If
    
    If Left(NewString, 6) = "02/29/" Then NewString = "02/28/" + Right(NewString, 4)
    
'   Set reformatted date to variable
    DateNumber = DateValue(NewString)

'   Output reformatted date
    ConvertDate = DateNumber
    
End Function

Private Function IsProcessRunning(pid As Long) As Boolean

    Dim hProcess As Long
    
    hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, 0, pid)
    CloseHandle hProcess
    IsProcessRunning = hProcess
    
End Function

Private Function KillProcess(pid As Long) As Boolean

    Dim hProcess As Long
    
    Do
    
        hProcess = OpenProcess(PROCESS_TERMINATE, 0, pid)
        KillProcess = TerminateProcess(hProcess, 0)
        CloseHandle hProcess
        
    Loop While IsProcessRunning(pid)
    
End Function

Private Function ApplicationActive(strApp As String) As Boolean

    ApplicationActive = True
    On Error Resume Next
    AppActivate strApp
    If Err.Number <> 0 Then ApplicationActive = False

End Function
